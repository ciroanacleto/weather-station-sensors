# Weather Station

This project is a weather station WiFi with moisture soil sensor, air humidity and temperature, atmospheric pressure.

A simple http webserver delivers the data.

The WiFi network ssi must to be changed at line 11 and the password at line 12 before to upload the code to Esp8266.

See also the project [smart-garden](https://bitbucket.org/ciroanacleto/smart-garden/src/master/)

This project consumes data from weater-station-sensors and persist into a timeseries database. This is not mandatory to put the weather station to work, but is cool. :)

