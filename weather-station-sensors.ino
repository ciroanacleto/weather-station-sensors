#include <Seeed_BME280.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <TimeLib.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <Adafruit_ADS1015.h>
 
const char *ssid = "<wifi_name>";
const char *password = "<password>";
const char *ntpServerName = "a.ntp.br";
const unsigned long sleepdelay = 60000;
const long offset = -10800;
const int MOISTURE_PIN = A0;
const int BATTERY_TRANSISTOR_PIN = D5;
const int SOLAR_TRANSISTOR_PIN = D6;
const int serverPort = 80;
const int adsAddress = 0x48;
const int airValue = 760;
const int watterValue = 399;

BME280 bme280;
WiFiUDP ntpUDP;
ESP8266WebServer server(serverPort);
NTPClient timeClient(ntpUDP, ntpServerName, offset, sleepdelay);
Adafruit_ADS1115 ads(adsAddress);

void setup() {
 
  Serial.begin(115200);

  // Init the transistor pin
  pinMode(BATTERY_TRANSISTOR_PIN, OUTPUT);
  pinMode(SOLAR_TRANSISTOR_PIN, OUTPUT);

  // start wifi subsystem
  WiFi.begin(ssid, password);
  reconnect();

  Serial.println("Mapping end-points ...");
  server.on("/getSensors", handleSensors);
  server.on("/sleep", handleSleep);
  server.on("/status", handleStatus);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("Server listening");

  Serial.println("Syncing NTP time.");
  timeClient.begin();
  timeClient.update();
  delay(500);
  Serial.println("Welcome, now is ");
  Serial.println(getDateTimeFormatted());

  Serial.println("Initializing BME280 ...");
  if (bme280.init()) {
    Serial.println("BME 280 connected.");
  } else {
    Serial.println("Device error!");
  }
 
}
 
void loop() {
  if (WiFi.status() == 3) {
    reconnect();
  }

  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }

 server.handleClient();
}
 
void handleSensors(){
  server.send(200, "application/json", getSensorsData());
}

void handleSleep(){
  if(server.hasArg("time")){
    server.send(200, "text/plain", "good bye");
    int time = server.arg("time").toInt();
    sleep(time);
  } else {
    server.send(400, "text/plain", "Bad request. Request have no params.");
  }
}

void handleNotFound(){
  server.send(404, "text/plain", "Not Found");
}

void handleStatus() {
  server.send(200, "text/plain" "I'm online and ready to work.");
}

String getSensorsData() {
  float temperature = bme280.getTemperature();
  float pressure = bme280.getPressure() / 100.0; // pressure in Pa
  float altitude = bme280.calcAltitude(bme280.getPressure());
  float humidity = bme280.getHumidity();
  int rawMoisture = analogRead(MOISTURE_PIN);
  int moisture = map(rawMoisture, airValue, watterValue, 0, 100);

  Serial.println("\nGetting sensors data ...");

  const int bufferSize = JSON_OBJECT_SIZE(11);
  DynamicJsonBuffer jsonBuffer(bufferSize);

  JsonObject &root = jsonBuffer.createObject();
  root.set("temperature", temperature);
  root.set("pressure", pressure);
  root.set("altitude", altitude);
  root.set("humidity", humidity);
  root.set("moisture", moisture);
  root.set("rawMoisture", rawMoisture);

  readBatteryVoltage(root);
  readSolarVoltage(root);

  root.set("time", getDateTimeFormatted());

  root.prettyPrintTo(Serial);

  String response = "";
  root.printTo(response);
  return response;
}

void readBatteryVoltage (JsonObject &root){

  digitalWrite(BATTERY_TRANSISTOR_PIN, HIGH);

  const float scalefactor = 0.2720f;
  int16_t rawADCvalue = ads.readADC_SingleEnded(2);
  float voltage = (rawADCvalue * scalefactor)/1000.0f;

  digitalWrite(BATTERY_TRANSISTOR_PIN, LOW);

  root.set("rawBatteryValue", rawADCvalue);
  root.set("batteryVoltage", voltage);
}

void readSolarVoltage (JsonObject &root){

  digitalWrite(SOLAR_TRANSISTOR_PIN, HIGH);

  const float scalefactor = 0.2641f;
  int16_t rawADCvalue = ads.readADC_Differential_0_1(); 
  float voltageOut = (rawADCvalue * scalefactor)/1000.0f;
  int resistor1 = 100000;
  int resistor2 = 330000;
  float solarVoltage = (voltageOut * (resistor1 + resistor2)) / resistor2;

  digitalWrite(SOLAR_TRANSISTOR_PIN, LOW);
  
  root.set("rawSolarValue", rawADCvalue);
  root.set("solarVoltage", solarVoltage);
}

void sleep (unsigned int seconds){
  Serial.print("\nSleeping for ");
  Serial.print(seconds);
  Serial.print(" see you soon!");

  delay(500);
  // deepSleep time is defined in microseconds. Multiply seconds by 1e6
  ESP.deepSleep(seconds * 1000000);
}

void reconnect() {

  // attempt to connect to the wifi if connection is lost
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Connecting to ");
    Serial.println(ssid);

    // loop while we wait for connection
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

    // print out some more debug once connected
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());
  }

}

String getDateTimeFormatted () {
  String formattedDate = timeClient.getFormattedTime();
  return formattedDate;
}
